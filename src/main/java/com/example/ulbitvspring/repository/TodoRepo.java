package com.example.ulbitvspring.repository;

import com.example.ulbitvspring.entity.TodoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TodoRepo extends JpaRepository<TodoEntity, Long> {
}
