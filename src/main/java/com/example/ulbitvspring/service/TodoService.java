package com.example.ulbitvspring.service;

import com.example.ulbitvspring.entity.TodoEntity;
import com.example.ulbitvspring.entity.UserEntity;
import com.example.ulbitvspring.model.TodoPOJO;
import com.example.ulbitvspring.repository.TodoRepo;
import com.example.ulbitvspring.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TodoService {

    @Autowired
    private TodoRepo todoRepo;

    @Autowired
    private UserRepo userRepo;

    public TodoPOJO createTodo(TodoEntity todo, Long userId) {
        UserEntity user =userRepo.findById(userId).get();
        todo.setUser(user);
        return TodoPOJO.toModel(todoRepo.save(todo));
    }

    public  TodoPOJO complete(Long id) {
        TodoEntity todoEntity = todoRepo.findById(id).get();
        todoEntity.setCompleted(!todoEntity.getCompleted());
        return TodoPOJO.toModel(todoRepo.save(todoEntity));
    }
}
