package com.example.ulbitvspring.service;

import com.example.ulbitvspring.entity.UserEntity;
import com.example.ulbitvspring.exception.UserAlreadyExistsException;
import com.example.ulbitvspring.exception.UserNotFoundException;
import com.example.ulbitvspring.model.UserPOJO;
import com.example.ulbitvspring.repository.UserRepo;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {


    private final UserRepo userRepo;

    public UserService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public UserEntity registration(UserEntity user) throws UserAlreadyExistsException {
        if (userRepo.findByUsername(user.getUsername()) != null) {
            throw new UserAlreadyExistsException("Пользователь с таким именем существует");
        }
        return userRepo.save(user);
    }

    public ResponseEntity<List<UserEntity>> findAll() {
        return ResponseEntity.ok(userRepo.findAll());
    }

    public UserPOJO getOne(Long id) throws UserNotFoundException {
        UserEntity userEntity = userRepo.findById(id).get();
        if (userEntity == null) {
            throw new UserNotFoundException("Пользователь не был найден");
        }
        return UserPOJO.toModel(userEntity);
    }

    public Long delete (Long id) {
        userRepo.deleteById(id);
        return id;
    }

}
