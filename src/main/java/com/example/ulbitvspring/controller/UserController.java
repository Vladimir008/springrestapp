package com.example.ulbitvspring.controller;

        import com.example.ulbitvspring.entity.UserEntity;
        import com.example.ulbitvspring.exception.UserAlreadyExistsException;
        import com.example.ulbitvspring.exception.UserNotFoundException;
        import com.example.ulbitvspring.repository.UserRepo;
        import com.example.ulbitvspring.service.UserService;
        import org.hibernate.engine.jdbc.spi.SqlExceptionHelper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.http.ResponseEntity;
        import org.springframework.web.bind.annotation.*;

        import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping()
    public ResponseEntity registration(@RequestBody UserEntity userEntity) {
        try {
            userService.registration(userEntity);
            return ResponseEntity.ok("Пользователь успешно сохранен!");
        } catch (UserAlreadyExistsException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Произошла ошибка!");
        }
    }

    @GetMapping
    public ResponseEntity getOneUser(@RequestParam Long id) {
        try {

            return ResponseEntity.ok(userService.getOne(id));
        } catch (UserNotFoundException e) {
            return  ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Произошла ошибка!");
        }
    }

    @GetMapping("/all")
    public ResponseEntity<List<UserEntity>> findAll() {
            return this.userService.findAll();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteUser(@PathVariable Long id) {
        try {
            return ResponseEntity.ok(userService.delete(id));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Произошла ошибка!");
        }
    }
}
