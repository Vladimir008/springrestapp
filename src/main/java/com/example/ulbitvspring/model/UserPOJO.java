package com.example.ulbitvspring.model;

import com.example.ulbitvspring.entity.TodoEntity;
import com.example.ulbitvspring.entity.UserEntity;

import java.util.List;
import java.util.stream.Collectors;

public class UserPOJO {
    private Long id;
    private String username;
    private List<TodoPOJO> todos;

    public static UserPOJO toModel(UserEntity userEntity) {
        UserPOJO model = new UserPOJO();
        model.setId(userEntity.getId());
        model.setUsername(userEntity.getUsername());
        model.setTodos(userEntity.getTodos().stream().map(TodoPOJO::toModel).collect(Collectors.toList()));
        return model;
    }

    public UserPOJO() {
    }

    public List<TodoPOJO> getTodos() {
        return todos;
    }

    public void setTodos(List<TodoPOJO> todos) {
        this.todos = todos;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
