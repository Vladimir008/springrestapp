package com.example.ulbitvspring.model;

import com.example.ulbitvspring.entity.TodoEntity;

public class TodoPOJO {
    private Long id;
    private  String title;
    private Boolean completed;

    public static TodoPOJO toModel(TodoEntity todoEntity) {
        TodoPOJO todoPOJO = new TodoPOJO();
        todoPOJO.setId(todoEntity.getId());
        todoPOJO.setTitle(todoEntity.getTitle());
        todoPOJO.setCompleted(todoEntity.getCompleted());

        return todoPOJO;
    }

    public TodoPOJO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

}
